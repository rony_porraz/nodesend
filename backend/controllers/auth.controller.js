const bcrypt = require("bcrypt")
const Usuarios = require("../models/Usuarios")
const jwt = require('jsonwebtoken')
const { validationResult } = require("express-validator")
require('dotenv').config({path:'variables.env'})


exports.authUsuario = async ( req , res , next ) =>
{

    // REVISAR ERRORES

    const errors = validationResult(req)

    if( !errors.isEmpty() ){
        return res.status(400).json({ errores: errors.array() })
    }

    // BUSCAR EL USUARIO EN LA DB

    const { email , password } = req.body

    const usuario = await Usuarios.findOne({ email })
    console.log(usuario);

    if( !usuario ){

        res.status(401).json({ msg: 'El usuario no existe' })
        return next()
    }

    // SI EL USUARIO EXISTE

    if( bcrypt.compareSync( password , usuario.password ) )
    {

        // CREAR JSON WEB TOKEN

        const token = jwt.sign({
            id: usuario._id,
            nombre: usuario.nombre,
            email: usuario.email
        }, process.env.SECRETA , { expiresIn: '8h' })

        console.log(token);
        console.log('password correcto');
        res.json({ token })

    }
    else
    {
        console.log('password incorrecto');
        res.status(401).json({ msg: 'Contrasela incorrecta' })
    }

    // VERIFICAR PASSWORD Y AUTENTICAR

}

exports.usuarioAutenticado = ( req, res , next ) =>
{
   console.log(req.usuario)
   res.json({ usuatioAuth : req.usuario });
}