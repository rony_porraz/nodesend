const Usuarios = require("../models/Usuarios");
const bcrypt = require('bcrypt')
const { validationResult }= require('express-validator')


exports.nuevoUsuario = async ( req , res ) =>
{

    // ERRORES EXPRESS VALIDATOR

    const errors = validationResult(req)

    if( !errors.isEmpty() ){
        return res.status(400).json({ errores : errors.array() })
    }

    // VERIFICAR USUARIO SI ESTA REGISTRADO
    const { email , password } = req.body

    let user = await Usuarios.findOne({ email })

    if( user )
    {
        return res.status(400).json({ msg: 'El usuario ya esta registrado' })
    }

    // CREAR USUARIOS

    user = new Usuarios(req.body)

    // HASHEAR PASSWORD

    const salt = await bcrypt.genSalt(10)

    user.password = await bcrypt.hash( password , salt )

    try
    {
        await user.save()

        res.send({ msg: 'Usuario creado correctamente' })
    }
    catch(e)
    {
        return res.status(400).json({ msg: 'Ocurrio un error en el servidor' })
    }

}