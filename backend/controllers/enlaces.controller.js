const Enlace = require("../models/Enlace");
const shortid = require('shortid')
const bcrypt = require("bcrypt");
const { validationResult } = require("express-validator");

exports.nuevoEnlace = async ( req, res ) =>
{

    // VALIDAR ERRORES
    const errors = validationResult(req)

    if( !errors.isEmpty() ){
        return res.status(400).json({ errores : errors.array() })
    }

    const { nombre_original, nombre } = req.body

    const enlace = new Enlace()

    enlace.url = shortid.generate()

    enlace.nombre = nombre

    enlace.nombre_original = nombre_original


    if( req.usuario ) // SI EL USUARIO ESTA AUTENTICADO
    {

        const { password , descargas } = req.body

        if( descargas ) // SI VIENE DESCARGAS
        {
            enlace.descargas = descargas
        }

        // SI VIENE PASSWORD
        if( password )
        {
            const salt = await bcrypt.genSalt(10)
            enlace.password = await bcrypt.hash( password , salt )
        }

        enlace.autor = req.usuario.id

    }

        try
        {

            enlace.save()

            res.json({ msg: `${enlace.url}` })

        }
        catch(e)
        {
        res.status(400).json({ msg: e })
        }

}

exports.todosEnlaces = async ( req, res ) => {
    try
    {
       const enlaces = await Enlace.find({  }).select('url -_id')
       res.json({enlaces})
    }
    catch (error)
    {
        res.status(400).json({ msg: error })
    }
}

exports.comprobar = async ( req , res ) => {

    const { pass1, pass2 } = req.body
    if( bcrypt.compareSync( pass1 , pass2 ) )
    {
       console.log('pasa');
       res.json({ passUrl: true })
    }
    else
    {
       console.log('no pasa');
       res.json({ passUrl: false })
    }

}

exports.obtenerEnlace = async ( req ,res , next ) =>
{

    const { url } = req.params // EL PARAMETRO DE LA RUTA

    // BUSCAMOS EL ENLACE EN LA BD
    const enlace = await Enlace.findOne({ url: url })

    if( enlace ) // SI EXISTE ENTRA
    {

        if( enlace.descargas <= 0 )
        {
           console.log('descargas queda 1');

           // AÑADIR AL OBJETO REQUESTS EL NOMBRE DEL ARCHIVO A BORRAR

           req.archivo = enlace.nombre

           // BORRAR DE LA BD

           await Enlace.findOneAndRemove( req.params.url )

           return next() // ENTRA AL SIG MIDDLEWARE

        }
        else // SI LAS DESCARGAS ES MAYOR 1
        {
            enlace.descargas-- // RESTAR DESCARGA
            await enlace.save() // GUARDAR EN LA DB
            console.log('quedan mas');
            return res.json({enlace})
        }
    }
    else
    {
        return res.status(404).json({ msg: 'El enlace no existe' })
    }

}