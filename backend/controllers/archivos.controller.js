const multer = require('multer');
const shortid = require('shortid');
const Enlaces = require('../models/Enlace');
const fs = require('fs')

exports.subirArchivo = async (req, res, next) => {

    const configuracionMulter = {
        limits : { fileSize : req.usuario ? 1024 * 1024 * 10 : 1024 * 1024 },
        storage: fileStorage = multer.diskStorage({
            destination: (req, file, cb) => {
                cb(null, __dirname+'/../uploads')
            },
            filename: (req, file, cb) => {
                const extension = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
                cb(null, `${shortid.generate()}${extension}` );
            }
        })
    }

    const upload = multer(configuracionMulter).single('archivo');

    upload( req, res, async (error) => {

        console.log(req.file);

        if(!error)
        {
            res.json({ archivo: req.file.filename  });
        }
        else
        {
            if( req.usuario )
            {
                res.status(400).json({ archivo: 'El archivo debe pesar minimo 10mb' });
            }
            else
            {

                res.status(400).json({ archivo: 'El archivo debe pesar minimo 1mb' });
            }
            //console.log(error);
            return next();
        }
    });

}


exports.borrarArchivo = async ( req, res , next ) => {

    const arch = req.archivo

    try
    {
       fs.unlinkSync(__dirname+`/../uploads/${arch}`)

       return res.json({msg: 'Ups, el archivo a sido eliminado'})
    }
    catch (error)
    {
      console.log('error al borrar el archivo');
      console.log(error);
    }

}

exports.descargarArchivo = async ( req , res ) => {

    const { archivo } = req.params

    const url = __dirname + '/../uploads/' + archivo

    console.log(url);

    res.download(url)

}