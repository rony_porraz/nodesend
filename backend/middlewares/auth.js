const jwt = require('jsonwebtoken')
require('dotenv').config({path:'variables.env'})

module.exports = ( req, res , next ) => {

    // OBTENER EL TOKEN

    const authHeader = req.get('authorization')

    if( authHeader )
    {
      console.log('entro');

      const token = authHeader.split(' ')[1] // LO LIMPIAMOS

      try
      {

        // COMPROBAR EL JWT

        const usuario = jwt.verify( token , process.env.SECRETA )

        console.log(usuario);

        req.usuario = usuario
        //res.json({usuariossss:usuario});

      }
      catch(e)
      {
        console.log(e);
        console.log('No hay token o header');
      }

    }

    return next()
}