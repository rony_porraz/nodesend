const express = require('express')
const conectarDB = require('./config/db')
const cors = require('cors')

// CREAR EL SERVER

const app = express()

conectarDB()

// CONFIGURACION DE EXPRESS

// PUERTO DEL APP
const port = process.env.PORT || 4000

// HABILITAR ENVIO DE JSON
app.use(express.json())
app.use( express.static('uploads') )

const opcionesCors = {
    origin: process.env.FRONT_URL
}

app.use( cors(opcionesCors) ) // habilitando cors

// ROUTING

app.use('/api/usuarios' , require('./routes/usuarios.routes'))
app.use('/api/auth' , require('./routes/auth.routes'))
app.use('/api/enlaces' , require('./routes/enlaces.routes'))
app.use('/api/archivos' , require('./routes/archivos.routes'))


app.listen( port , () => {

    console.log('listo');

})