const express = require('express')
const router = express.Router()
const { check } = require('express-validator')
const { borrarArchivo } = require('../controllers/archivos.controller')
const { nuevoEnlace , obtenerEnlace , todosEnlaces , comprobar } = require('../controllers/enlaces.controller')
const auth = require('../middlewares/auth')
router.post( '/' ,
    [
       check('nombre' , 'Sube un archivo').not().isEmpty(),
       check('nombre_original' , 'Sube un archivo').not().isEmpty()
    ],
    auth , nuevoEnlace )

router.get('/' , todosEnlaces )

router.get('/:url' , obtenerEnlace , borrarArchivo )

router.post('/comprobar' , comprobar )


module.exports = router