const express = require('express')
const router = express.Router()
const { check } = require('express-validator')
const { authUsuario , usuarioAutenticado } = require('../controllers/auth.controller')
const auth = require('../middlewares/auth')


// ROUTES

router.post( '/' ,
    [
        check('email' , 'Agrega un email valido').isEmail(),
        check('password' , 'La contraseña es obligatoria').not().isEmpty()
    ]
   ,  authUsuario )


router.get( '/' , auth  ,  usuarioAutenticado )

module.exports= router