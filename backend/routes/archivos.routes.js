const express = require('express')
const router = express.Router()
const { check } = require('express-validator')
const { subirArchivo , descargarArchivo } = require('../controllers/archivos.controller')
const auth = require('../middlewares/auth')

// SUBIDA DE ARCHIVOS

router.post( '/' , auth , subirArchivo )

router.get( '/:archivo' , descargarArchivo )

//router.delete( '/:id' , eliminarArchivo )

module.exports = router