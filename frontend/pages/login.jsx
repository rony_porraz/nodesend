import React , { useContext , useEffect }  from 'react'
import Layout from '../components/Layout'
import { Button, Image ,  Form, Segment, Message } from 'semantic-ui-react'
import styled from '@emotion/styled'
import { useFormik } from 'formik'
import * as yup from 'yup'
import authContext from '../context/auth/authContext'
import Router from 'next/router'

const ContForm = styled.div`
    max-width: 500px;
    margin: 0 auto;
    margin-top: 3rem;
`

const ErrorC = ({ err }) => {
    return (
        <div class="ui message">
            <div class="header">{ err }</div>
        </div>
    )
}

const Logo = styled.img`
   width: 40%;
   display:block;
   margin: 20px auto;
`

const login = () => {

    const { iniciarSesion , autenticado } = useContext(authContext)

    useEffect( () => {

        if(autenticado){
            Router.push('/')
        }

    }, [autenticado])

    // VALIDACION CON FORMIK

    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: yup.object({
            email: yup.string().email('El email no es valido').required('el email es obligatorio'),
            password: yup.string().required('La contraseña es requerida').min(6, "La contraseña debe tener al menos 6 caracteres")
        }),
        onSubmit: (v) => {
            iniciarSesion( v )
        }
    })

    return (
        <Layout>

        <ContForm>

            <h1 className="text-center"> Iniciar Sesión </h1>

            <Segment className="mt-4">

                <Form onSubmit={formik.handleSubmit}>

                <Logo src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Nextjs-logo.svg/800px-Nextjs-logo.svg.png" />

                    <Form.Field>
                        <label>Email</label>
                        <input
                            id="email"
                            placeholder='Tu email'
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                        />
                    </Form.Field>

                    {
                        formik.touched.email && formik.errors.email
                            ?
                            <ErrorC err={formik.errors.email}/>
                            : null
                    }


                    <Form.Field>
                        <label>Contraseña</label>
                        <input
                            type="password"
                            placeholder='Tu contraseña'
                            id="password"
                            value={formik.values.password}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                        />
                    </Form.Field>

                    {
                        formik.touched.password && formik.errors.password
                            ?
                            <ErrorC err={formik.errors.password}/>
                            : null
                    }


                    <Button type='submit' fluid secondary size="large"> Iniciar Sesión </Button>

                </Form>

            </Segment>

        </ContForm>

    </Layout>

    )
}

export default login
