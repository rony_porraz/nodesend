import Layout from "../components/Layout"
import { Button, Header, Icon, Segment, Grid, Image, Card } from 'semantic-ui-react'
import styled from '@emotion/styled'
import authContext from "../context/auth/authContext"
import React, { useContext , useState ,useEffect } from 'react'
import tokenAuth from "../config/tokenAuth"
import Link from "next/link"
import Dropzone from '../components/Dropzone'
import appContext from "../context/app/appContext"
import Formulario from "../components/Formulario"

const W100 = styled.div`
   width: 1000px;
   margin:0 auto;
   display: grid;
   grid-template-columns: 50% 50%;
   background-color:#e5e5e5;
   box-sizing:border-box;
   padding: 1.5rem;
   height: 700px;
   border-radius: 4px;
   .ui.placeholder.segment
   {
     margin: 0 !important;
   }
   .ui.card, .ui.cards>.card{
     width: 100%;
   }
`

const Descripcion = styled.p`
   font-size: 18px;
   line-height: 2em;
   margin-top: 1rem;
`
const Titulo = styled.h1`
   font-size: 35px;
`

export default function Home() {

  const { token , usuarioAutenticado } = useContext(authContext)
  const { url } = useContext(appContext)

  useEffect( () => {

    const token = localStorage.getItem('token')
    if( token ){
      usuarioAutenticado()
    }

  }, [] )

  return (
    <Layout>

      <W100 divided='vertically' >

        {
          url
          ?
          <>
          <div style={{paddingRight:'1.5rem'}} className="d-flex justify-content-center align-content-center flex-column">
            <h1 className="mb-2 text-center"> Tu URL es </h1>
             <Button className="d-block" onClick={() => navigator.clipboard.writeText(`${process.env.frontendUrl}/enlaces/${url}`) } size="big" fluid secondary> { `${process.env.frontendUrl}/enlaces/${url}` } </Button>
          </div>
          </>
          : <Dropzone/>
        }

        <Segment placeholder className="alineararriba" >

          <Titulo> Compartir Archivos De Forma Sensilla Y Privada </Titulo>

          <Descripcion>

            Lorem ipsum dolor sit amet consectetur, adipisicing Delectus porro, ab  laudantium enim facere tenetur neque aliquam aut quia culpa aspernatur at ducimus vero vitae sed provident dolorum tempora.
            lorem

            <Link href="/registro"> Crea una cuenta </Link>

          </Descripcion>

        </Segment>

      </W100>

    </Layout>

  )
}
