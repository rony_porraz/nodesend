import Layout from "../../components/Layout"
import React, { useState } from 'react'
import { Button, Header, Icon, Segment, Form } from 'semantic-ui-react'
import styled from "@emotion/styled"
import clienteAxios from '../../config/axios'
import swal from 'sweetalert'

const W100 = styled.div`
   width: 1000px;
   margin:0 auto;
   display: flex;
   grid-template-columns: 50% 50%;
   background-color:#e5e5e5;
   box-sizing:border-box;
   padding: 1.5rem;
   height: 700px;
   border-radius: 4px;
   .ui.placeholder.segment
   {
     margin: 0 !important;
   }
   .ui.card, .ui.cards>.card{
     width: 100%;
   }
`


export async function getStaticProps(props) {

  const enlace = props.params.enlace

  const resultado = await clienteAxios.get(`api/enlaces/${enlace}`)

  console.log(resultado);

  return {
    props: {
      enlace: resultado.data
    }
  }

}
export async function getStaticPaths() {

  const enlaces = await clienteAxios.get('/api/enlaces')

  console.log('xd');

  return {
    paths: enlaces.data.enlaces.map(enl => ({
      params: { enlace: enl.url }
    })),
    fallback: false
  }

}

const Enlace = ({ enlace }) => {

  console.log(enlace.enlace.password);
  const [ passTuyo , setPass ] = useState(enlace.enlace.password)

  const comprobar = async () => {
     try
     {
       const { data:{ passUrl } } = await clienteAxios.post('/api/enlaces/comprobar' , { pass1: passTuyo , pass2: enlace.enlace.password })
       console.log(passUrl);
       if( passUrl ){
          setPass(false)
       }
       else
       {
        swal("Hey!", `Contraseña Incorrecta`, "error", {
          button: "Ok!",
        });
       }
     }
     catch (error)
     {
       throw new Error(error)
     }
  }

  if (passTuyo) {
    return (
      <Layout>
        <Segment placeholder className="w-100">
          <Form >
            <Form.Field>
              <label>El Archivo Tiene Contraseña</label>
              <input placeholder='Contraseña' onChange={ e => setPass(e.target.value) } />
            </Form.Field>
            <Button type='submit' onClick={comprobar} >Verificar</Button>
          </Form>
        </Segment>
      </Layout>
    )
  }else{
    return (

      <Layout>

        <W100 divided='vertically' >

          <Segment placeholder className="w-100">
            <Header icon>
              <Icon name='pdf file outline' />
              Descarga Tu Archivo {enlace.enlace.nombre_original}
            </Header>
            <a download href={`${process.env.backendUrl}/api/archivos/${enlace.enlace.nombre}`} style={{ color: '#fff !important' }}>
              <Button primary>
                Descargar
                </Button>
            </a>
          </Segment>

        </W100>

      </Layout>
    )

  }
}

export default Enlace
