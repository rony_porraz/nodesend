import React, { useContext, useEffect , useState } from 'react'
import Layout from '../components/Layout'
import { Button, Image ,  Form, Segment, Message } from 'semantic-ui-react'
import styled from '@emotion/styled'
import { useFormik } from 'formik'
import * as yup from 'yup'
import authContext from '../context/auth/authContext'

const ContForm = styled.div`
    max-width: 500px;
    margin: 0 auto;
    margin-top: 3rem;
`

const ErrorC = ({ err }) => {
    return (
        <div className="ui message">
            <div className="header">{ err }</div>
        </div>
    )
}

const Logo = styled.img`
   width: 40%;
   display:block;
   margin: 20px auto;
`

const registro = () => {


    const { mensaje , usuario , registrarUsuario  } = useContext(authContext)
    const [ loadingState, setLoadingState ] = useState(false)

    useEffect( () => {


    },[])


    // VALIDACION CON FORMIK

    const formik = useFormik({
        initialValues: { // STATE INICIAL DEL FORMULARIO
            nombre: '',
            email: '',
            password: ''
        },
        validationSchema: yup.object({ // VALIDACION DE CAMPOS
            nombre: yup.string().required('El nombre es obligatorio'),
            email: yup.string().email('El email no es valido').required('el email es obligatorio'),
            password: yup.string().required('La contraseña es requerida').min(6, "La contraseña debe tener al menos 6 caracteres")
        }),
        onSubmit: async (v , { resetForm }) => {
            //console.log(v);
            setLoadingState(true) // CAMBIO EL STADO A CARGANDO Y EL BOTON SE BLOQUEA
            await registrarUsuario( v , resetForm ) // MANDO LOS DATOS AL CONTEXT Y ESPERO
            setLoadingState(false) // CAMBIO EL LOADING A FALSE
        }
    })


    return (
        <Layout>

            <ContForm>

                <h1 className="text-center"> Crear Cuenta </h1>

                <Segment className="mt-4">

                    <Form onSubmit={formik.handleSubmit}>

                    {usuario}

                    <Logo src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Nextjs-logo.svg/800px-Nextjs-logo.svg.png" />

                        <Form.Field>
                            <label>Nombre</label>
                            <input
                                id="nombre"
                                autoComplete="off"
                                placeholder='Tu nombre'
                                value={formik.values.nombre}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                            />
                        </Form.Field>

                        {
                            formik.touched.nombre && formik.errors.nombre
                                ?
                                <ErrorC err={formik.errors.nombre}/>
                                : null
                        }

                        <Form.Field>
                            <label>Email</label>
                            <input
                                id="email"
                                placeholder='Tu email'
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                            />
                        </Form.Field>

                        {
                            formik.touched.email && formik.errors.email
                                ?
                                <ErrorC err={formik.errors.email}/>
                                : null
                        }


                        <Form.Field>
                            <label>Contraseña</label>
                            <input
                                type="password"
                                placeholder='Tu contraseña'
                                id="password"
                                value={formik.values.password}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                            />
                        </Form.Field>

                        {
                            formik.touched.password && formik.errors.password
                                ?
                                <ErrorC err={formik.errors.password}/>
                                : null
                        }


                        <Button type='submit' loading={ loadingState ? true : false }  fluid secondary size="large"> Crear Cuenta </Button>

                    </Form>

                </Segment>

            </ContForm>

        </Layout>

    )
}

export default registro
