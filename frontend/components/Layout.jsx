import React from 'react'
import Head from 'next/head'
import 'semantic-ui-css/semantic.min.css'
import { Container } from 'semantic-ui-react'
import Header from './Header'

const Layout = ({ children }) => {
    return (
        <>

        <Head>
            <title> Next Send </title>
        </Head>

        <Container>

          <Header/>

          { children }

        </Container>

        </>
    )
}

export default Layout
