import React, { useState, useCallback, useContext } from 'react'
import { useDropzone } from 'react-dropzone'
import { Segment, Header, Icon, Button, Label, Card, Image } from 'semantic-ui-react'
import appContext from '../context/app/appContext'
import authContext from '../context/auth/authContext'
import Formulario from './Formulario'

const Dropzone = () => {

    // APP CONTEXT
    const { mostrarAlerta, subirArchivos, loading, crearEnlace } = useContext(appContext)

    const onDropRejected = () => {
        mostrarAlerta('No puedes subir un archivo tan pesado', 'err')
    }

    // AUTH CONTEXT
    const { autenticado } = useContext(authContext)

    console.log(autenticado);

    const onDropAccepted = useCallback(async (files) => {

        // CREAR FORM DATA

        const formdata = new FormData()

        formdata.append('archivo', files[0])

        subirArchivos(formdata, files[0].path)

    }, [])

    // EXTRAER CONTENIDO DE DROPZONE

    const { getRootProps, getInputProps, acceptedFiles } = useDropzone({ onDropAccepted, onDropRejected, maxSize: 1000000 })

    const archivos = acceptedFiles.map(file => (
        <Card>
            <Card.Content>
                <Card.Header> {file.path} </Card.Header>
                <Label className="mt-2">
                    <Icon name='file' /> {(file.size / Math.pow(1024, 2)).toFixed(2)} MB
                    </Label>
            </Card.Content>
            <Card.Content extra >
                <Button onClick={crearEnlace} className="corregirxd" secondary fluid content="Generar Link" loading={loading ? true : false} />
            </Card.Content>
        </Card>
    ))

    return (
        <Segment placeholder>

            {
                acceptedFiles.length > 0
                    ?
                    <>

                        <h1 className="text-center mb-3">Archivos</h1>

                        {
                            autenticado
                                ?
                                <div className="mb-3 w-100">
                                    <Formulario />
                                </div>
                                : null
                        }

                        <ul style={{ listStyle: 'none', padding: '0' }}>
                            {
                                archivos
                            }
                        </ul>

                    </>
                    :
                    <>

                        <Header icon>

                            <Icon name='pdf file outline' />

                    Selecciona Un Archivo Para Subir

                    </Header>

                        <div {...getRootProps({ className: "p-3 w-100 dropzone" })} >

                            <input {...getInputProps()} />

                            <Button primary fluid>Añadir Archivo</Button>

                        </div>

                    </>
            }

        </Segment>
    )
}

export default Dropzone
