import React, { useState, useContext } from 'react'
import { Dropdown , Label , Form , Checkbox } from 'semantic-ui-react'
import appContext from '../context/app/appContext'


const Formulario = () => {

  const { cambiarPassword , cambiarDescargas } = useContext( appContext )

   const [ hasPass , setPass ] = useState(false)

    const countryOptions = [
        { key: 'af', value: '1', text: '1 Descargas' },
        { key: 'ax', value: '5', text: '5 Descargas' },
        { key: 'al', value: '10', text: '10 Descargas' },
        { key: 'dz', value: '20', text: '20 Descargas' },
      ]

    return (
    <Form className="w-100">

      <div>

          <Label color='black' horizontal className="mb-2">
            Eliminar Despues De
          </Label>

      <Dropdown fluid  selection placeholder='Selecciona El N° De Descargas' options={countryOptions} onChange={ ( e , { value } ) => cambiarDescargas( parseInt(value) )  } />

      </div>

      <div className="mt-2">

        <Checkbox label='Agregar Contraseña' className="mt-2" onChange={() => setPass(!hasPass) } />

        {
          hasPass
          ?
            <input className="mt-2" placeholder='Crea una contraseña' onChange={ e => cambiarPassword(e.target.value) } />
          : null
        }

      </div>

    </Form>
    )

}

export default Formulario
