import React, { useContext, useEffect } from 'react'
import { Button, Icon } from 'semantic-ui-react'
import Link from 'next/link'
import authContext from '../context/auth/authContext'
import appContext from '../context/app/appContext'

const Header = () => {

  // AUTH CONTEXT

  const { usuario, cerrarSesion } = useContext(authContext)

  // APP CONTEXT

  const { reiniciarState } = useContext( appContext )

  return (

    <header className="d-flex justify-content-between align-items-center pt-4 pb-5">

      <Link href="/" >
        <h1 onClick={ reiniciarState } style={{ cursor: 'pointer' }}>Next Send</h1>
      </Link>

      <div className="contain_button d-flex align-items-center">

        {
          !usuario
            ?
            <>
              <Link href="/login" >
                <Button content="Iniciar Sesión" secondary />
              </Link>
              <Link href="/registro" >
                <Button content="Crear Cuenta" color="blue" />
              </Link>
            </>
            :
            <>
              <h2 style={{ marginBottom: '0' }} className="mr-2" > Hola {usuario.nombre} </h2>
              <Button color='facebook' onClick={cerrarSesion} >
                Logout
              </Button>
            </>
        }

      </div>

    </header>

  )
}

export default Header
