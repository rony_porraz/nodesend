import React, { useReducer } from 'react'
import appContext from './appContext'
import { CAMBIAR_DESCARGAS , REINICIAR_STATE , SUBIR_ENLACE_EXITO , CAMBIAR_PASS , CREAR_ENLACE_EXITO , SUBIR_ARCHIVO } from '../../types'
import appReducer from './appReducer'
import swal from 'sweetalert'
import clienteAxios from '../../config/axios'

const AppState = ({ children }) => {

    const initial = {
        nombre: '',
        nombre_original: '',
        loading: false,
        descargas: 1,
        password: '',
        autor: null,
        url: ''
    }

    const [ state , dispatch ] = useReducer( appReducer ,  initial )

    // MOSTRAR ALERTA

    const mostrarAlerta = ( msg , type ) => {
        switch( type ){
            case 'err':
                swal("Hey!", `${msg}`, "error", {
                    button: "Ok!",
                });
        }
    }

    // SUBIR ARCHIVO

    const subirArchivos = async ( formdata , n_original ) => {

        dispatch({
            type: SUBIR_ARCHIVO,
        })

        try
        {

            const { data: { archivo } } = await clienteAxios.post( '/api/archivos', formdata )

            console.log(archivo)

            dispatch({
                type: SUBIR_ENLACE_EXITO,
                payload: {
                    nombre: archivo,
                    nombre_original: n_original
                }
            })

            alert('subido xd')

        }
        catch (error)
        {

            const { archivo } = error.response.data
            mostrarAlerta( `${archivo}` , 'err' )

        }

    }

    // CREAR ENLACE

    const crearEnlace = async () =>
    {
        const _data = {
            nombre: state.nombre,
            nombre_original: state.nombre_original,
            descargas: state.descargas,
            password: state.password,
            autor: state.autor
        }

        try
        {

            const { data: { msg } } = await clienteAxios.post('/api/enlaces', _data )
            dispatch({
                type: CREAR_ENLACE_EXITO,
                payload: msg
            })
            console.log(msg);

        }
        catch(e)
        {

        }

    }

    const reiniciarState = () => dispatch({ type: REINICIAR_STATE })

    const cambiarPassword = valor => dispatch({ type: CAMBIAR_PASS , payload: valor })
    const cambiarDescargas = valor => dispatch({ type: CAMBIAR_DESCARGAS , payload: valor })

    return (
       <appContext.Provider
        value={{
            nombre:state.nombre,
            nombre_original:state.nombre_original,
            loading: state.loading,
            descargas: state.descargas,
            password: state.password,
            autor: state.autor,
            url: state.url,
            mostrarAlerta,
            subirArchivos,
            crearEnlace,
            reiniciarState,
            cambiarPassword,
            cambiarDescargas
        }}
       >
           { children }
       </appContext.Provider>
   )
}

export default AppState