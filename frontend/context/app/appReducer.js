import { REINICIAR_STATE, SUBIR_ENLACE_EXITO , CREAR_ENLACE_EXITO , SUBIR_ARCHIVO , CAMBIAR_PASS , CAMBIAR_DESCARGAS } from '../../types'

export default ( state , action ) => {
    switch( action.type ){
        case SUBIR_ARCHIVO:
            return {
                ...state,
                loading: true
            }
        case SUBIR_ENLACE_EXITO:
            return {
                ...state,
                nombre: action.payload.nombre,
                nombre_original: action.payload.nombre_original,
                loading: false
            }
        case CREAR_ENLACE_EXITO:
            return {
                ...state,
                url: action.payload
            }
        case CAMBIAR_DESCARGAS:
            return {
                ...state,
                descargas: action.payload
            }
        case REINICIAR_STATE:
            return {
                ...state,
                nombre: '',
                nombre_original: '',
                loading: false,
                descargas: 1,
                password: '',
                autor: null,
                url: ''
            }
        case CAMBIAR_PASS:
            return {
                ...state,
                password: action.payload
            }
        default:
            return state
    }
}