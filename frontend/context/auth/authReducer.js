import { USUARIO_AUTENTICADO , LOGIN_EXITOSO , CERRAR_SESION } from "../../types"

export default ( state, action ) => {
    switch( action.type ){
        case USUARIO_AUTENTICADO:
            console.log(action.payload);
            return {
                ...state,
                autenticado:true,
                usuario: action.payload
            }
        case LOGIN_EXITOSO:
            localStorage.setItem('token' , action.payload)
            return {
                ...state,
                token: action.payload,
                autenticado: true
            }
        case CERRAR_SESION:
            localStorage.removeItem('token')
            return {
                ...state,
                usuario: null,
                token: null,
                autenticado: false
            }
        default:
            return state
    }
}