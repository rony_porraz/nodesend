import React , { useReducer } from 'react'
import Router from 'next/router'
import { REINICIAR_STATE, USUARIO_AUTENTICADO , LOGIN_EXITOSO, CERRAR_SESION } from '../../types'
import authContext from './authContext'
import authReducer from './authReducer'
import clienteAxios from '../../config/axios'
import swal from 'sweetalert'
import tokenAuth from '../../config/tokenAuth'

const AuthState = ({ children }) => {

    // STATE DE LA APP

    const initialState =  {
        token: typeof window !== 'undefined' ?  localStorage.getItem('token') : null,
        autenticado: null,
        usuario: null,
        mensaje: "xxxxxxx"
    }

    // REDUCER

    const [ state , dispatch ] = useReducer( authReducer , initialState )


    // INICIAR SESION

    const iniciarSesion = async datos => {

        try
        {
            const { data: { token } } = await clienteAxios.post('/api/auth' , datos)
            dispatch({
                type: LOGIN_EXITOSO,
                payload: token
            })
        }
        catch (error)
        {
           const { msg } = error.response.data
           return alertFn('err' , `¡${msg}!`)
        }

    }

    // REGISTRAR USUARIOS

    const registrarUsuario = async ( datos , cb ) => {
        console.log('xd');
        try
        {
          const { data: { msg } } = await clienteAxios.post('/api/usuarios' , datos)
          cb()
          Router.push('/login')
          return alertFn('ok' , `¡${msg}!`)
        }
        catch(e)
        {
            console.log(e.response);
            const { msg } = e.response.data
            return alertFn('err' , `¡${msg}!`)
        }
    }

    // BUSCAR USUARIO AUTENTICADO

    const usuarioAutenticado = async () => {

        console.log('desde auth');
        const token = localStorage.getItem('token')

        if( token ){
            tokenAuth(token)
        }

        try
        {

            const { data: { usuatioAuth } } = await clienteAxios.get('/api/auth')

            console.log(usuatioAuth);

            if( usuatioAuth ){
                dispatch({
                    type: USUARIO_AUTENTICADO,
                    payload: usuatioAuth
                })
            }

        }
        catch (error)
        {
            console.log(error);
        }

        /*dispatch({
            type: USUARIO_AUTENTICADO,
            payload: nombre
        })*/

    }


    const alertFn = ( type , msg ) => {
        switch( type ){
            case 'ok':
            swal({
                title: msg,
                icon: "success",
            });
            break
            case 'err':
            swal({
                title: msg,
                icon: "error",
            });
            break
        }
    }

    const cerrarSesion = () => dispatch({type: CERRAR_SESION})

    return (

        <authContext.Provider
          value={{
             token: state.token,
             usuario: state.usuario,
             autenticado: state.autenticado,
             mensaje: state.mensaje,
             usuarioAutenticado,
             registrarUsuario,
             iniciarSesion,
             cerrarSesion
          }}
        >

            {children}

        </authContext.Provider>

    )
}

export default AuthState